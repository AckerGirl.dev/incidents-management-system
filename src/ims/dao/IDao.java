package ims.dao;

import java.util.List;

import ims.db.IMSDBException;

public interface IDao<T, E> {
	/**
	 * @param user
	 * @throws IMSDBException
	 */
	public void create(T entity) throws IMSDBException;

	/**
	 * @param id
	 * @return
	 * @throws IMSDBException
	 */
	@SuppressWarnings("hiding")
	public <T> E read(int id) throws IMSDBException;
	
	/**
	 * @return
	 * @throws IMSDBException
	 */
	public List<T> readOne(int id) throws IMSDBException;
	
	@SuppressWarnings("hiding")
	public <T> E readBy(String login, String password) throws IMSDBException;

	/**
	 * @param user
	 * @throws IMSDBException
	 */
	public void update(T entity) throws IMSDBException;

	/**
	 * @param id
	 * @throws IMSDBException
	 */
	public void delete(Integer id) throws IMSDBException;

	/**
	 * @return
	 * @throws IMSDBException
	 */
	public List<T> list() throws IMSDBException;
	
	/**
	 * @param id
	 * @return
	 * @throws IMSDBException
	 */
	public List<T> list(Integer id) throws IMSDBException;
}
