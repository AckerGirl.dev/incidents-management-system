package ims;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class AuthenticationTest extends Application {

	private Stage primaryStage;
	private BorderPane authUI;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Authentication");
		
		try {
			// Load root layout from fxml file.
			authUI = FXMLLoader.load(getClass().getResource("ui/AuthenticationUI.fxml"));

			// Show the scene containing the root layout.
			Scene scene = new Scene(authUI);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setResizable(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
