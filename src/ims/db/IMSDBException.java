package ims.db;

import java.sql.SQLException;

public class IMSDBException extends SQLException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IMSDBException(String message) {
		super(message);
	}
}
