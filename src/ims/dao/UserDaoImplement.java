package ims.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ims.db.DB_Connection;
import ims.db.IMSDBException;
import ims.model.User;


public class UserDaoImplement implements IDao<User, User> {
	
	@Override
	public void create(User user) throws IMSDBException {
		// TODO Auto-generated method stub
		try {
			Session session = DB_Connection.getInstance().getSession();
			// Creating Transaction Object 
			Transaction transaction = session.beginTransaction();
			session.save(user);
			// Transaction Is Committed To Database
			transaction.commit();
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}

	}

	@Override
	public User read(int id) throws IMSDBException {
		// TODO Auto-generated method stub
		User user = null;
		try {
			Session session = DB_Connection.getInstance().getSession();
			user = session.find(User.class, id);
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
		
		return user;
	}
	
	@Override
	public List<User> readOne(int id) throws IMSDBException {
		// TODO Auto-generated method stub
		List<User> users = new ArrayList<>();
		User user = null;
		try {
			
			Session session = DB_Connection.getInstance().getSession();
			user = session.find(User.class, id);
			users.add(user);
					
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
				
		return users;
	}

	@Override
	public User readBy(String login, String password) throws IMSDBException {
		// TODO Auto-generated method stub
		User user = null;
		
		try {
			Session session = DB_Connection.getInstance().getSession();
			Query query = session.createQuery(
				     "From User u WHERE u.login = '" + login + "' AND u.password = '" + password + "'");
			user = (User) query.getResultList().get(0);
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
		
		return user;
	}
	
	@Override
	public void update(User user) throws IMSDBException {
		// TODO Auto-generated method stub
		try {
			Session session = DB_Connection.getInstance().getSession();
			
			//Creating Transaction Object 
			Transaction transaction = session.beginTransaction();
			session.update(user);
			
			// Transaction Is Committed To Database
			transaction.commit();
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
	}

	@Override
	public void delete(Integer id) throws IMSDBException {
		// TODO Auto-generated method stub
		try {
			
			User user = read(id);
			
			if (user != null) {
				Session session = DB_Connection.getInstance().getSession();
				
				// Creating Transaction Object
				Transaction transaction = session.beginTransaction();
				
				session.delete(user);
				// Transaction Is Committed To Database
				transaction.commit();
			}
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> list() throws IMSDBException {
		// TODO Auto-generated method stub
		List<User> users = new ArrayList<>();
		try {
			Session session = DB_Connection.getInstance().getSession();
		
			Query query = session.createQuery("From User");
			users = query.getResultList();
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
		
		return users;
	}

	@Override
	public List<User> list(Integer id) throws IMSDBException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
