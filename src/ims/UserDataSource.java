package ims;

import java.util.List;

import ims.dao.IDao;
import ims.dao.UserDaoImplement;
import ims.db.IMSDBException;
import ims.model.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class UserDataSource {
	ObservableList<User> users = FXCollections.observableArrayList();
	
	@SuppressWarnings("rawtypes")
	private IDao dao;

	public UserDataSource() throws IMSDBException {
		dao = new UserDaoImplement();
		listUsers();
	}

	public ObservableList<User> getUsers() {
		return users;
	}

	@SuppressWarnings("unchecked")
	public void createUser(User user) throws IMSDBException {
		dao.create(user);
		this.users.add(user);
	}

	@SuppressWarnings("unchecked")
	public void updateUser(User user, int selectedIndex) throws IMSDBException {
		dao.update(user);
		users.remove(selectedIndex);
		users.add(selectedIndex, user);
	}

	private void listUsers() throws IMSDBException {
		@SuppressWarnings("unchecked")
		List<User> users = this.dao.list();
		this.users.addAll(users);
	}

	public void deleteUser(int selectedIndex) throws IMSDBException {
		User user = users.get(selectedIndex);
		users.remove(user);
		dao.delete(user.getId());
	}

	public User readUser(Integer id) throws IMSDBException {
		return (User) dao.read(id);
	}
	
	public List<User> readOneUser(Integer id) throws IMSDBException {
		@SuppressWarnings("unchecked")
		List<User> users = this.dao.readOne(id);
		return users;
	}
	
	public User readUserBy(String login, String password) throws IMSDBException {
		return (User) dao.readBy(login, password);
	}
	
}
