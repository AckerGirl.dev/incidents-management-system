package ims;

import java.util.List;

import ims.dao.IDao;
import ims.dao.IncidentDaoImplement;
import ims.db.IMSDBException;
import ims.model.Incident;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class IncidentDataSource {
	ObservableList<Incident> incidents = FXCollections.observableArrayList();
	
	@SuppressWarnings("rawtypes")
	private IDao dao;

	public IncidentDataSource() throws IMSDBException {
		dao = new IncidentDaoImplement();
		listIncidents();
	}
	
	public ObservableList<Incident> getIncidents() {
		return incidents;
	}

	@SuppressWarnings("unchecked")
	public void createIncident(Incident incident) throws IMSDBException {
		dao.create(incident);
		this.incidents.add(incident);
	}
	
	@SuppressWarnings("unchecked")
	public void updateIncident(Incident incident, int selectedIndex) throws IMSDBException {
		dao.update(incident);
		incidents.remove(selectedIndex);
		incidents.add(selectedIndex, incident);
	}
	
	public void listIncidents(int id) throws IMSDBException {
		@SuppressWarnings("unchecked")
		List<Incident> incidents = this.dao.list(id);
		this.incidents.addAll(incidents);
	}
	
	private void listIncidents() throws IMSDBException {
		@SuppressWarnings("unchecked")
		List<Incident> incidents = this.dao.list();
		this.incidents.addAll(incidents);
	}
	
	public void deleteIncident(int selectedIndex) throws IMSDBException {
		Incident incident = incidents.get(selectedIndex);
		incidents.remove(incident);
		dao.delete(incident.getId());
	}
	
	public Incident readIncident(Integer id) throws IMSDBException {
		return (Incident) dao.read(id);
	}
	
}
