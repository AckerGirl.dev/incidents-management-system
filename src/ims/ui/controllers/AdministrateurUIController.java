package ims.ui.controllers;

import ims.IMSRuntime;
import ims.db.IMSDBException;
import ims.model.Role;
import ims.model.User;

import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

public class AdministrateurUIController {
	
	@FXML
	private TableView<User> userTable;
	@FXML
	private TableColumn<User, String> nomColumn;
	@FXML
	private TableColumn<User, String> prenomColumn;
	@FXML
	private TableColumn<User, String> telephoneColumn;
	@FXML
	private TableColumn<User, String> emailColumn;
	@FXML
	private TableColumn<User, String> loginColumn;
	@FXML
	private TableColumn<User, String> passwordColumn;
	@FXML
	private TableColumn<User, String> roleColumn;
	@FXML
	private TableColumn<User, String> idColumn;

	@FXML
	private Button btnNew;
	@FXML
	private Button btnNewUser;
	@FXML
	private Button btnEdit;
	@FXML
	private Button btnList;
	@FXML
	private Button btnUpdateUser;
	@FXML
	private Button btnAddNewUser;
	@FXML
	private Button btnSearch;
	@FXML
	private Button btnRefresh;
	@FXML
	private Button btnDelete;
	@FXML
	private Button btnProfile;
	@FXML
	private Button btnLogout;
	@FXML
	private Label lblStatus;
	@FXML
	private Label lblStatusMini;
	@FXML
	private Pane pnlStatus;
	
	@FXML
	private GridPane pnUsers;
	@FXML
	private GridPane pnNewUser;
	@FXML
	private Button btnAddUser;
	@FXML
	private GridPane pnEditUser;
	
	@FXML
	private ComboBox<String> roleComboBox;
	private User user;
	@FXML
	private TextField nomField;
	@FXML
	private TextField prenomField;
	@FXML
	private TextField emailField;
	@FXML
	private TextField telephoneField;
	@FXML
	private TextField loginField;
	@FXML
	private PasswordField passwordField;
	private Stage dialogStage;
	
	@FXML
	private ComboBox<String> roleComboBoxEdit;
	@FXML
	private TextField nomFieldEdit;
	@FXML
	private TextField prenomFieldEdit;
	@FXML
	private TextField emailFieldEdit;
	@FXML
	private TextField telephoneFieldEdit;
	@FXML
	private TextField loginFieldEdit;
	@FXML
	private PasswordField passwordFieldEdit;
	private int selectedIndex;
	@FXML
	private TextField searchField;
	
	public AdministrateurUIController() {
	}

	@FXML
	private void initialize() {
		
		btnProfile.setText(AuthUIController.getAuthenticatedUser().getNom() + " " 
				+ AuthUIController.getAuthenticatedUser().getPrenom());
		
		refreshTable();
		pnUsers.toFront();
		initializeRoleComboBox();
	}
	
	@FXML
	private void handleClicks (ActionEvent event) {
		if(event.getSource() == btnNew || event.getSource() == btnNewUser)  {
			
			//pnlStatus.setBackground(new BackgroundFill(Color.rgb(63, 43, 99), CornerRadii.EMPTY, Insets.EMPTY));
			showNewUserPanel();
			
		} else if(event.getSource() == btnEdit)  {
			
			showEditUserPanel();
		
		} else if(event.getSource() == btnList)  {
			
			showAllUsersPanel();
			
		}
	}
	
	@FXML
	private void handle_btnAddUser() throws IMSDBException {
		User user = new User();
		
		if (isInputValid()) {
			user.setNom(new String(nomField.getText()));
			user.setPrenom(new String(prenomField.getText()));
			user.setEmail(new String(emailField.getText()));
			user.setTelephone(new String(telephoneField.getText()));
			user.setLogin(new String(loginField.getText()));
			user.setPassword(new String(passwordField.getText()));
			user.setRole(new String(roleComboBox.getSelectionModel().getSelectedItem()));
			
			IMSRuntime.getInstance().getDataSource().createUser(user);
			initFields();
			
			showAllUsersPanel();
		}
	}
	
	@FXML
	private void handle_btnUpdateUser() throws IMSDBException {
		
			user.setNom(new String(nomFieldEdit.getText()));
			user.setPrenom(new String(prenomFieldEdit.getText()));
			user.setEmail(new String(emailFieldEdit.getText()));
			user.setTelephone(new String(telephoneFieldEdit.getText()));
			user.setLogin(new String(loginFieldEdit.getText()));
			user.setPassword(new String(passwordFieldEdit.getText()));
			user.setRole(new String(roleComboBoxEdit.getSelectionModel().getSelectedItem()));
				
			IMSRuntime.getInstance().getDataSource().updateUser(this.user, this.selectedIndex);
			userTable.refresh();
			userTable.getSelectionModel().select(selectedIndex);
				
			showAllUsersPanel();
	}

	/**
	 * Called when the user clicks on the Supprimer button.
	 * 
	 * @throws UMSDBException
	 */
	@FXML
	private void handleDeleteUser() throws IMSDBException {
		int selectedIndex = userTable.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0) {
			IMSRuntime.getInstance().getDataSource().deleteUser(selectedIndex);
			;
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning ...");
			alert.setHeaderText("Alert !");
			alert.setContentText("Please, select the user to delete ! !");
			alert.showAndWait();
		}
	}
	
	@FXML
	private void handleSearchUser() throws IMSDBException {

		try {
			User user = IMSRuntime.getInstance().getDataSource().readUser(Integer.parseInt(searchField.getText()));
			if (user == null) {
				IMSRuntime.alert(AlertType.INFORMATION, "Search user", null, "User doesn't exist, enter a valid ID !");
			} else {
				// IMSRuntime.getInstance().showUserEditUI(user);
			}

		} catch (NumberFormatException e) {
			IMSRuntime.alert(AlertType.ERROR, e.getClass().toString(), null,
					"Please enter a number !");
		}

	}
	
	@FXML
	private void handle_btnLogout() {
		System.out.println(AuthUIController.getAuthenticatedUser().getNom() + " " + AuthUIController.getAuthenticatedUser().getPrenom()
				+ " logout successfully !");
		AuthUIController.setAuthenticatedUser(null);
		IMSRuntime.getInstance().showAuthUI();
	}
	
	public void showNewUserPanel() {
		lblStatus.setText("New User");
		lblStatusMini.setText("Home / New user");
		pnNewUser.toFront();
	}
	
	public void showEditUserPanel() {
		User selectedUser = userTable.getSelectionModel().getSelectedItem();
		selectedIndex = userTable.getSelectionModel().getSelectedIndex();
		if(selectedUser != null) {
			
			setUser(selectedUser);
			this.user = selectedUser;
			
			lblStatus.setText("Edit User");
			lblStatusMini.setText("Home / Edit user");
			pnEditUser.toFront();
			
		} else {
			IMSRuntime.alert(AlertType.WARNING, "Any selection", "Any user selected !",
					"Please, select the user to edit !");
		}
	}
	
	public void showAllUsersPanel() {
		lblStatus.setText("All Users");
		lblStatusMini.setText("Home / All users");
		pnUsers.toFront();
	}
	
	private void initializeRoleComboBox() {
		roleComboBox.getItems().clear();
		roleComboBoxEdit.getItems().clear();
		
		roleComboBox.getItems().addAll(
				Role.ADMINISTRATEUR.getValue(), 
				Role.RAPPORTEUR.getValue(),
				Role.RESPONSABLE.getValue(),
				Role.DEVELOPPEUR.getValue()
		);
		
		roleComboBoxEdit.getItems().addAll(
				Role.ADMINISTRATEUR.getValue(), 
				Role.RAPPORTEUR.getValue(),
				Role.RESPONSABLE.getValue(),
				Role.DEVELOPPEUR.getValue()
		);
	}
	
	public void initFields() {
		nomField.setText("");
		prenomField.setText("");
		emailField.setText("");
		telephoneField.setText("");
		loginField.setText("");
		passwordField.setText("");
		roleComboBox.getSelectionModel().select("");
	}
	
	// Validates the user input in the text fields.
	private boolean isInputValid() {
		String errorMessage = "";

		if (nomField.getText() == null || nomField.getText().length() == 0) {
				errorMessage += "Lastname not assigned !\n";
		}
		if (prenomField.getText() == null || prenomField.getText().length() == 0) {
			errorMessage += "Firstname not assigned !\n";
		}
		if (emailField.getText() == null || emailField.getText().length() == 0) {
			errorMessage += "Email not assigned !\n";
		}
		if (telephoneField.getText() == null || telephoneField.getText().length() == 0) {
			errorMessage += "Telephone not assigned !\n";
		}
		if (loginField.getText() == null || loginField.getText().length() == 0) {
			errorMessage += "Login not assigned !\n";
		}
		if (passwordField.getText() == null || passwordField.getText().length() == 0) {
			errorMessage += "Password not assigned !\n";
		}

		if (errorMessage.length() == 0)
			return true;
		else { // Show the error message.
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(dialogStage);
			alert.setTitle("Fields not set or invalid !");
			alert.setHeaderText("Please, try to enter valid input !");
			alert.setContentText(errorMessage);

			alert.showAndWait();
			return false;
		}
	}
	
	public void setUser(User user) {
		
		nomFieldEdit.setText(user.getNom());
		prenomFieldEdit.setText(user.getPrenom());
		emailFieldEdit.setText(user.getEmail());
		telephoneFieldEdit.setText(user.getTelephone());
		loginFieldEdit.setText(user.getLogin());
		passwordFieldEdit.setText(user.getPassword());
		roleComboBoxEdit.getSelectionModel().select(user.getRole());
	}
	
	@FXML
	private void refreshTable() {
		nomColumn.setCellValueFactory(cellData -> new SimpleStringProperty( cellData.getValue().getNom()));
		prenomColumn.setCellValueFactory(cellData ->  new SimpleStringProperty(cellData.getValue().getPrenom()));
		telephoneColumn.setCellValueFactory(cellData -> new SimpleStringProperty( cellData.getValue().getTelephone()));
		emailColumn.setCellValueFactory(cellData ->  new SimpleStringProperty(cellData.getValue().getEmail()));
		loginColumn.setCellValueFactory(cellData -> new SimpleStringProperty( cellData.getValue().getLogin()));
		passwordColumn.setCellValueFactory(cellData ->  new SimpleStringProperty(cellData.getValue().getPassword()));
		roleColumn.setCellValueFactory(cellData -> new SimpleStringProperty( cellData.getValue().getRole()));
		
		userTable.setItems(IMSRuntime.getInstance().getDataSource().getUsers());
	}
	
}
