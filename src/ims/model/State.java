package ims.model;

public enum State {
	NOUVEAU("Nouveau"),
	ASSIGNE("Assigne"),
	EN_ATTENTE("En attente"),
	RESOLU("Resolu"),
	RE_OUVERT("Re-ouvert"),
	CLOTURE("Cloture");

	private String name;

	private State(String name) {
		this.name = name;
	}

	public String getValue() {
		return this.name;

	}
}
