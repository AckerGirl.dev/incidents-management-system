package ims.ui.controllers;

import ims.IMSRuntime;
import ims.db.IMSDBException;
import ims.model.Incident;
import ims.model.Note;
import ims.model.State;
import ims.model.User;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

public class RapporteurUIController {
	
	Incident incident;
	AuthUIController authUIController;
	@FXML
	private TableView<Incident> incidentTable;
	@FXML
	private TableColumn<Incident, String> descriptionColumn;
	@FXML
	private TableColumn<Incident, String> stateColumn;
	@FXML
	private TableColumn<Incident, String> opendateColumn;
	@FXML
	private TableColumn<Incident, String> idColumn;
	
	@FXML
	private TableView<Note> commentsTable;
	@FXML
	private TableColumn<Note, String> messageColumn;
	@FXML
	private TableColumn<User, String> userColumn;
	
	@FXML
	private ComboBox<String> stateComboBox;
	@FXML
	private ComboBox<String> stateComboBoxEdit;
	
	@FXML
	private Button btnNew;
	@FXML
	private Button btnEdit;
	@FXML
	private Button btnDashboard;
	@FXML
	private Button btnList;
	@FXML
	private Button btnComments;
	@FXML
	private Button btnDelete;
	@FXML
	private Button btnProfile;
	@FXML
	private Label lblStatus;
	@FXML
	private Label lblStatusMini;
	
	@FXML
	private GridPane pnIncidents;
	@FXML
	private GridPane pnNewIncident;
	@FXML
	private GridPane pnDashboard;
	@FXML
	private GridPane pnEditIncident;
	@FXML
	private GridPane pnComments;
	
	@FXML
	private TextField searchField;
	@FXML
	private TextArea descriptionArea;
	@FXML
	private TextArea messageArea;
	@FXML
	private TextArea descriptionAreaEdit;
	private int selectedIndex;
	private Stage dialogStage;
	@FXML
	private Button btnCreateIncident;
	@FXML
	private Button btnUpdateIncident;
	@FXML
	private Button btnSendComment;
	@FXML
	private Button btnLogout;
	
	public RapporteurUIController() {
	}
	
	@FXML
	private void initialize() {
		authUIController = new AuthUIController();
		btnProfile.setText(AuthUIController.getAuthenticatedUser().getNom() + " " 
				+ AuthUIController.getAuthenticatedUser().getPrenom());
		
		refreshTable();
		initializeStateComboBox();
		displayComments();
		pnDashboard.toFront();
	}
	
	@FXML
	private void handleClicks (ActionEvent event) {
		if(event.getSource() == btnNew)  {
			
			//pnlStatus.setBackground(new BackgroundFill(Color.rgb(63, 43, 99), CornerRadii.EMPTY, Insets.EMPTY));
			selectedIndex = -1;
			showNewIncidentPanel();
			
		} else if(event.getSource() == btnList)  {
			
			showAllIncidentsPanel();
			
		} else if(event.getSource() == btnDashboard)  {
			
			selectedIndex = -1;
			showDashboardPanel();
			
		} else if(event.getSource() == btnEdit)  {
			
			showEditIncidentPanel();
			
		} else if(event.getSource() == btnComments)  {
			
			showCommentsPanel();
			
		}
	}
	
	@FXML
	private void handle_btnCreateIncident() throws IMSDBException {
		Incident incident = new Incident();
		
		if (isInputValid()) {
			incident.setDescription(new String(descriptionArea.getText()));
			incident.setUser(AuthUIController.getAuthenticatedUser());
			incident.setState(new String(stateComboBox.getSelectionModel().getSelectedItem()));
			
			IMSRuntime.getInstance().getIncidentDataSource().createIncident(incident);
			initFields();
			
			showAllIncidentsPanel();
		}
	}
	
	@FXML
	private void handle_btnUpdateUser() throws IMSDBException {
		
			incident.setDescription(new String(descriptionAreaEdit.getText()));
			incident.setState(new String(stateComboBoxEdit.getSelectionModel().getSelectedItem()));
				
			IMSRuntime.getInstance().getIncidentDataSource().updateIncident(this.incident, this.selectedIndex);
			incidentTable.refresh();
			incidentTable.getSelectionModel().select(selectedIndex);
				
			showAllIncidentsPanel();
	}
	
	@FXML
	private void handleDeleteIncident() throws IMSDBException {
		int selectedIndex = incidentTable.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0) {
			IMSRuntime.getInstance().getIncidentDataSource().deleteIncident(selectedIndex);
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning ...");
			alert.setHeaderText("Alert !");
			alert.setContentText("Please, select the incident to delete !");
			alert.showAndWait();
			
		}
	}
	
	@FXML
	private void handleSearchIncident() throws IMSDBException {

		try {
			Incident incident = IMSRuntime.getInstance().getIncidentDataSource().readIncident(Integer.parseInt(searchField.getText()));
			if (incident == null) {
				IMSRuntime.alert(AlertType.INFORMATION, "Search Incident", null, "Incident doesn't exist, enter a valid ID !");
			} else {
				// IMSRuntime.getInstance().showUserEditUI(user);
			}

		} catch (NumberFormatException e) {
			IMSRuntime.alert(AlertType.ERROR, e.getClass().toString(), null,
					"Please enter a number !");
		}

	}
	
	@FXML
	private void handle_btnLogout() {
		System.out.println(AuthUIController.getAuthenticatedUser().getNom() + " " + AuthUIController.getAuthenticatedUser().getPrenom()
				+ " logout successfully !");
		AuthUIController.setAuthenticatedUser(null);
		IMSRuntime.getInstance().showAuthUI();
	}
	
	@FXML
	private void handle_btnSendComment() throws IMSDBException {
		Note note = new Note();
		
		if (messageArea.getText() != null || messageArea.getText().length() != 0) {
			
			Incident selectedIncident = incidentTable.getSelectionModel().getSelectedItem();
			selectedIndex = incidentTable.getSelectionModel().getSelectedIndex();
			if(selectedIncident != null) {
				// create note associated to incident
				note.setMessage((new String(messageArea.getText())));
				note.setUser(AuthUIController.getAuthenticatedUser());
				note.setIncident(selectedIncident);
				IMSRuntime.getInstance().getNoteDataSource().createNote(note);
				
				//init the message area
				messageArea.setText("");
				
				displayComments();
				
				// Add the note in the incident note List
				setIncident(selectedIncident);
				this.incident = selectedIncident;
				this.incident.getNote().add(note);
				
			} else {
				IMSRuntime.alert(AlertType.WARNING, "Any selection", "Any incident selected !",
						"Please, select the incident to comment !");
			}

		} else { 
			// Show the error message.
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(dialogStage);
			alert.setTitle("Field not set or invalid !");
			alert.setHeaderText(" Please, try to enter valid input !");
			alert.setContentText("Message not set !");

			alert.showAndWait();
		}
	}
	
	private void displayComments() {
		
		messageColumn.setCellValueFactory(cellData -> new SimpleStringProperty( cellData.getValue().getMessage()));
		commentsTable.setItems(IMSRuntime.getInstance().getNoteDataSource().getNotes());
	}
	
	public void initFields() {
		descriptionArea.setText("");
		messageArea.setText("");
		stateComboBox.getSelectionModel().select(stateComboBox.getPromptText());
	}
	
	private boolean isInputValid() {
		String errorMessage = "";

		if (descriptionArea.getText() == null || descriptionArea.getText().length() == 0) {
				errorMessage += "Incident description not set !\n";
		}

		if (errorMessage.length() == 0)
			return true;
		else { // Show the error message.
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(dialogStage);
			alert.setTitle("Field not set or invalid !");
			alert.setHeaderText(" Please, try to enter valid input !");
			alert.setContentText(errorMessage);

			alert.showAndWait();
			return false;
		}
	}
	
	public void showNewIncidentPanel() {
		lblStatus.setText("Create Ticket");
		lblStatusMini.setText("Home / New Ticket");
		pnNewIncident.toFront();
	}
	
	public void showAllIncidentsPanel() {
		lblStatus.setText("My Tickets");
		lblStatusMini.setText("Home / My Tickets");
		pnIncidents.toFront();
	}
	
	public void showDashboardPanel() {
		lblStatus.setText("Dashboard");
		lblStatusMini.setText("Home / Dashboard");
		pnDashboard.toFront();
	}
	
	public void showEditIncidentPanel() {
		Incident selectedIncident = incidentTable.getSelectionModel().getSelectedItem();
		selectedIndex = incidentTable.getSelectionModel().getSelectedIndex();
		if(selectedIncident != null) {
			
			setIncident(selectedIncident);
			this.incident = selectedIncident;
			
			lblStatus.setText("Edit User");
			lblStatusMini.setText("Home / Edit user");
			pnEditIncident.toFront();
			
		} else {
			IMSRuntime.alert(AlertType.WARNING, "Any selection", "Any incident selected !",
					"Please, select the incident to edit !");
		}
	}
	
	public void showCommentsPanel() {
		lblStatus.setText("Comments");
		lblStatusMini.setText("Home / Comments");
		pnComments.toFront();
	}
	
	public void setIncident(Incident incident) {
		
		descriptionAreaEdit.setText(incident.getDescription());
		stateComboBoxEdit.getSelectionModel().select(incident.getState());
	}

	private void initializeStateComboBox() {
		stateComboBox.getItems().clear();
		stateComboBoxEdit.getItems().clear();
		
		stateComboBox.getItems().addAll(
				State.NOUVEAU.getValue(),
				State.ASSIGNE.getValue(),
				State.EN_ATTENTE.getValue(),
				State.RE_OUVERT.getValue(),
				State.RESOLU.getValue(),
				State.CLOTURE.getValue()
		);
		
		stateComboBoxEdit.getItems().addAll(
				State.NOUVEAU.getValue(),
				State.EN_ATTENTE.getValue(),
				State.RE_OUVERT.getValue()
		);
	}
	
	private void refreshTable() {
		descriptionColumn.setCellValueFactory(cellData -> new SimpleStringProperty( cellData.getValue().getDescription()));
		opendateColumn.setCellValueFactory(cellData ->  new SimpleStringProperty(cellData.getValue().getOpenDate()));
		stateColumn.setCellValueFactory(cellData -> new SimpleStringProperty( cellData.getValue().getState()));
		
		incidentTable.setItems(IMSRuntime.getInstance().getIncidentDataSource().getIncidents());
	}
	
}
