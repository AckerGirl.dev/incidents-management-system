package ims;

import java.io.IOException;
import java.util.Optional;

import ims.UserDataSource;
import ims.IMSRuntime;
import ims.db.IMSDBException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class IMSRuntime extends Application {
	
	private Stage primaryStage;
	private BorderPane mainUI, authUI;
	private AnchorPane adminUI, reporterUI;
	private UserDataSource userDataSource;
	private IncidentDataSource incidentDataSource;
	private NoteDataSource noteDataSource;
	private static IMSRuntime instance;

	@Override
	public void start(Stage primaryStage) {
		
		instance = this;
		
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Incident Management System");
		this.primaryStage.setResizable(false);

		try {
			userDataSource = new UserDataSource();
			incidentDataSource = new IncidentDataSource();
			noteDataSource = new NoteDataSource();
			
			showAuthUI();
			
		} catch (IMSDBException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}

	// Shows the user UI inside the root layout.
	public void showAdminUI() {
		try {
			mainUI = (BorderPane) FXMLLoader.load(getClass().getResource("ui/MainUI.fxml"));

			// Show the scene containing the root layout.
			Scene scene = new Scene(mainUI, 1322, 809);
			primaryStage.setScene(scene);
			primaryStage.show();
			// Load userUI
			adminUI = (AnchorPane) FXMLLoader.load(getClass().getResource("ui/AdministrateurUI.fxml"));
			// Set userUI into the center of root layout.
			mainUI.setCenter(adminUI);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void showReporterUI() {
		try {
			mainUI = (BorderPane) FXMLLoader.load(getClass().getResource("ui/MainUI.fxml"));

			// Show the scene containing the root layout.
			Scene scene = new Scene(mainUI, 1322, 809);
			primaryStage.setScene(scene);
			primaryStage.show();
			
			reporterUI = (AnchorPane) FXMLLoader.load(getClass().getResource("ui/RapporteurUI.fxml"));
			mainUI.setCenter(reporterUI);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void showAuthUI() {
		try {
			
			mainUI = (BorderPane) FXMLLoader.load(getClass().getResource("ui/MainUI.fxml"));

			// Show the scene containing the root layout.
			Scene scene = new Scene(mainUI, 705, 465);
			primaryStage.setScene(scene);
			primaryStage.show();
			
			authUI = (BorderPane) FXMLLoader.load(getClass().getResource("ui/AuthenticationUI.fxml"));
			mainUI.setCenter(authUI);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Returns the main stage.
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static IMSRuntime getInstance() {
		return instance;
	}

	public UserDataSource getDataSource() {
		return userDataSource;
	}
	
	public IncidentDataSource getIncidentDataSource() {
		return incidentDataSource;
	}
	
	public NoteDataSource getNoteDataSource() {
		return noteDataSource;
	}
	
	public static Optional<ButtonType> alert(AlertType alertType, String title, String headerText, String contentText) {
		Alert alert = new Alert(alertType);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);
		return alert.showAndWait();
	}


}
