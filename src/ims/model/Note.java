package ims.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity (name="Note")
public class Note {
	@ManyToOne(cascade=CascadeType.PERSIST)
	private User user;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	private String message;
	private LocalDateTime date;
	@ManyToOne(cascade=CascadeType.PERSIST)
	private Incident incident;
	
	public Note(User user, int id, String message, String date) {
		super();
		this.user = user;
		this.id = id;
		this.message = message;
		this.setDate();
	}
	
	public Note(User user, String message, String date) {
		super();
		this.user = user;
		this.message = message;
		this.setDate();
	}
	
	public Note() {
		this(null, "", "");
		this.setDate();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDate() {
		return DateTimeFormatter.ofPattern("MM-dd-yyyy hh:mm:ss", Locale.ENGLISH).format(date);
	}

	public void setDate() {
		this.date = LocalDateTime.now();
	}
	
	public Incident getIncident() {
		return incident;
	}

	public void setIncident(Incident incident) {
		this.incident = incident;
	}
}
