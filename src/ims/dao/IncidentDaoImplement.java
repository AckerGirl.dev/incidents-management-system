package ims.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import ims.db.DB_Connection;
import ims.db.IMSDBException;
import ims.model.Incident;

public class IncidentDaoImplement implements IDao<Incident, Incident> {

	@Override
	public void create(Incident entity) throws IMSDBException {
		// TODO Auto-generated method stub
		try {
			Session session = DB_Connection.getInstance().getSession();
			// Creating Transaction Object 
			Transaction transaction = session.beginTransaction();
			session.save(entity);
			// Transaction Is Committed To Database
			transaction.commit();
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
	}

	@Override
	public Incident read(int id) throws IMSDBException {
		// TODO Auto-generated method stub
		Incident incident = null;
		try {
			Session session = DB_Connection.getInstance().getSession();
			incident = session.find(Incident.class, id);
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
		
		return incident;
	}

	@Override
	public void update(Incident entity) throws IMSDBException {
		// TODO Auto-generated method stub
		try {
			Session session = DB_Connection.getInstance().getSession();
			
			//Creating Transaction Object 
			Transaction transaction = session.beginTransaction();
			session.update(entity);
			
			// Transaction Is Committed To Database
			transaction.commit();
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
	}

	@Override
	public void delete(Integer id) throws IMSDBException {
		// TODO Auto-generated method stub
		try {
			
			Incident incident = read(id);
			
			if (incident != null) {
				Session session = DB_Connection.getInstance().getSession();
				
				// Creating Transaction Object
				Transaction transaction = session.beginTransaction();
				
				session.delete(incident);
				// Transaction Is Committed To Database
				transaction.commit();
			}
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Incident> list() throws IMSDBException {
		// TODO Auto-generated method stub
		List<Incident> incidents = new ArrayList<>();
		try {
			Session session = DB_Connection.getInstance().getSession();
		
			Query query = session.createQuery("From Incident");
			incidents = query.getResultList();
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
		
		return incidents;
	}

	@Override
	public <T> Incident readBy(String login, String password) throws IMSDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Incident> readOne(int id) throws IMSDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Incident> list(Integer id) throws IMSDBException {
		// TODO Auto-generated method stub
		List<Incident> incidents = new ArrayList<>();
		try {
			Session session = DB_Connection.getInstance().getSession();
		
			Query query = session.createQuery(
				     "From Incident i WHERE i.user_id = '" + id + "'");
			incidents = query.getResultList();
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
		
		return incidents;
	}
	
}
