package ims.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity (name="Incident")
public class Incident {
	
	@OneToOne
	private User user;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	private String description;
	private String state;
	private LocalDateTime openDate;
	
	@OneToMany
	private List<Note> note;
	
	public Incident(int id, User user, String description, State state, String openDate) {
		super();
		this.user = user;
		this.id = id;
		this.description = description;
		this.setOpenDate();
		this.note = new ArrayList<Note>();
		
		this.state = new String(state.getValue());
	}
	
	public Incident(User user, String description, State state) {
		super();
		this.user = user;
		this.description = description;
		this.setOpenDate();
		this.note = new ArrayList<Note>();
		
		this.state = new String(state.getValue());
	}
	
	public Incident() {
		this(null, "", State.NOUVEAU);
		this.setOpenDate();
		this.note = new ArrayList<Note>();
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getOpenDate() {
		return DateTimeFormatter.ofPattern("MM-dd-yyyy hh:mm:ss", Locale.ENGLISH).format(openDate);
	}

	public void setOpenDate() {
		this.openDate = LocalDateTime.now();
	}
	
	public List<Note> getNote() {
		return this.note;
	}
	
}
