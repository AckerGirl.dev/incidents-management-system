package ims;

import java.util.List;

import ims.dao.IDao;
import ims.dao.NoteDaoImplement;
import ims.db.IMSDBException;
import ims.model.Note;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class NoteDataSource {
	ObservableList<Note> notes = FXCollections.observableArrayList();
	
	@SuppressWarnings("rawtypes")
	private IDao dao;

	public NoteDataSource() throws IMSDBException {
		dao = new NoteDaoImplement();
		listNotes();
	}
	
	public ObservableList<Note> getNotes() {
		return notes;
	}

	@SuppressWarnings("unchecked")
	public void createNote(Note note) throws IMSDBException {
		dao.create(note);
		this.notes.add(note);
	}
	
	@SuppressWarnings("unchecked")
	public void updateNote(Note note, int selectedIndex) throws IMSDBException {
		dao.update(note);
		notes.remove(selectedIndex);
		notes.add(selectedIndex, note);
	}
	
	public void listNotes(int id) throws IMSDBException {
		@SuppressWarnings("unchecked")
		List<Note> notes = this.dao.list(id);
		this.notes.addAll(notes);
	}
	
	@SuppressWarnings("unused")
	private void listNotes() throws IMSDBException {
		@SuppressWarnings("unchecked")
		List<Note> notes = this.dao.list();
		this.notes.addAll(notes);
	}
	
	public void deleteNote(int selectedIndex) throws IMSDBException {
		Note note = notes.get(selectedIndex);
		notes.remove(note);
		dao.delete(note.getId());
	}
	
	public Note readNote(Integer id) throws IMSDBException {
		return (Note) dao.read(id);
	}
	
}
