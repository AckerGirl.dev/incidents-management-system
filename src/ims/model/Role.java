package ims.model;

public enum Role {
	ADMINISTRATEUR("Administrateur"),
	RESPONSABLE("Responsable"),
	RAPPORTEUR("Rapporteur"),
	DEVELOPPEUR("Developpeur");

	private String name;

	private Role(String name) {
		this.name = name;
	}

	public String getValue() {
		return this.name;

	}
}
