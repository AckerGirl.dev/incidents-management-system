package ims.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class DB_Connection {
	private static DB_Connection instance = new DB_Connection();
	private Session session = null;
	private SessionFactory factory = null;
	
	private DB_Connection() {
		StandardServiceRegistry ssr = 
		new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		
		// Creating Hibernate Session Factory Instance
		factory = meta.getSessionFactoryBuilder().build();
		
		// Creating The Hibernate's Session Object
		session = factory.openSession();
	}
	
	public Session getSession () {
		if (session == null) 
			new DB_Connection();
		return session;
	}
	
	public void closeSession() {
		if (session != null) {
			session.close();
			session = null;
		}
	}
	
	public void closeSessionFactory() {
		if (factory != null) {
			closeSession ();
			factory.close();
			factory = null;
		}
	}
	
	public static DB_Connection getInstance() {
		return instance;
	}
}
