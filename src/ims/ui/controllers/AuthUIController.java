package ims.ui.controllers;

import ims.IMSRuntime;
import ims.UserDataSource;
import ims.db.IMSDBException;
import ims.model.Role;
import ims.model.User;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class AuthUIController{
	
	UserDataSource ds;
	private static User authenticatedUser;
	@FXML
	private TextField login;
	@FXML
	private PasswordField password;
	@FXML
	private Label errorLabel;
	
	public AuthUIController() {
	}
	
	public static User getAuthenticatedUser() {
		return authenticatedUser;
	}
	
	public static void setAuthenticatedUser(User user) {
		AuthUIController.authenticatedUser = user;
	}

	@FXML
	private void handleLogin() throws IMSDBException {
		User user = new User();
		
		try {
			ds = new UserDataSource();
			user = ds.readUserBy(login.getText(), password.getText());
			if (user.getLogin().equals(login.getText()) && user.getPassword().equals(password.getText())) {
				setAuthenticatedUser(user);
					
				if(user.getRole().equals(Role.ADMINISTRATEUR.getValue())) {
					IMSRuntime.getInstance().showAdminUI();
					
				} else if(user.getRole().equals(Role.RAPPORTEUR.getValue())) {
					IMSRuntime.getInstance().showReporterUI();
				}
				
				System.out.println("success ! " + authenticatedUser.getNom());
				
			} else {
				errorLabel.setVisible(true);
				login.setText("");
				password.setText("");
			}

		} catch (NumberFormatException | IMSDBException e) {
			e.getMessage();
		}
	}
	
}
