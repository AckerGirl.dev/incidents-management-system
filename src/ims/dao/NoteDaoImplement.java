package ims.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import ims.db.DB_Connection;
import ims.db.IMSDBException;
import ims.model.Note;

public class NoteDaoImplement implements IDao<Note, Note> {

	@Override
	public void create(Note entity) throws IMSDBException {
		// TODO Auto-generated method stub
		try {
			Session session = DB_Connection.getInstance().getSession();
			// Creating Transaction Object 
			Transaction transaction = session.beginTransaction();
			session.save(entity);
			// Transaction Is Committed To Database
			transaction.commit();
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
	}

	@Override
	public Note read(int id) throws IMSDBException {
		// TODO Auto-generated method stub
		Note note = null;
		try {
			Session session = DB_Connection.getInstance().getSession();
			note = session.find(Note.class, id);
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
		
		return note;
	}

	@Override
	public void update(Note entity) throws IMSDBException {
		// TODO Auto-generated method stub
		try {
			Session session = DB_Connection.getInstance().getSession();
			
			//Creating Transaction Object 
			Transaction transaction = session.beginTransaction();
			session.update(entity);
			
			// Transaction Is Committed To Database
			transaction.commit();
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
	}

	@Override
	public void delete(Integer id) throws IMSDBException {
		// TODO Auto-generated method stub
		try {
			
			Note note = read(id);
			
			if (note != null) {
				Session session = DB_Connection.getInstance().getSession();
				
				// Creating Transaction Object
				Transaction transaction = session.beginTransaction();
				
				session.delete(note);
				// Transaction Is Committed To Database
				transaction.commit();
			}
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Note> list() throws IMSDBException {
		// TODO Auto-generated method stub
		List<Note> notes = new ArrayList<>();
		try {
			Session session = DB_Connection.getInstance().getSession();
		
			Query query = session.createQuery("From Note");
			notes = query.getResultList();
			
		} catch (Exception e) {
			throw new IMSDBException("ERROR:" + e.getClass() + ":" + e.getMessage());
		}
		
		return notes;
	}

	@Override
	public <T> Note readBy(String login, String password) throws IMSDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Note> readOne(int id) throws IMSDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Note> list(Integer id) throws IMSDBException {
		// TODO Auto-generated method stub
		return null;
	}

}
